{

  "host": "0.0.0.0",
  "port": 7777,

  "keyLength": 10,

  "maxLength": 400000,

  "staticMaxAge": 86400,

  "recompressStaticAssets": true,

  "logging": [
    {
      "level": "verbose",
      "type": "Console",
      "colorize": true
    }
  ],

  "keyGenerator": {
    "type": "phonetic"
  },

  "rateLimits": {
    "categories": {
      "normal": {
        "totalRequests": 500,
        "every": 60000
      }
    }
  },

  "storage": {
  "type": "redis",
  "host": "redis://h:p553c812bdce60733c1d306b1eb18bb422256d91b9916b8504fc6a75c389af8cd@ec2-54-163-250-167.compute-1.amazonaws.com:17019",
  "port": 17019,
  "db": 2
},

  "documents": {
    "about": "./about.md"
  }

}
